.. currentmodule:: pglift.conf

PostgreSQL configuration
========================

.. autofunction:: make
.. autofunction:: info
