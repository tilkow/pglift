.. currentmodule:: pglift.manifest

Resource manifests
==================

.. autoclass:: Manifest
    :members:
.. autoclass:: Instance
    :members:
.. autoclass:: InstanceState
    :members:
