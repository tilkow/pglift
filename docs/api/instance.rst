.. currentmodule:: pglift.instance

Instance operations
===================

.. autofunction:: apply
.. autofunction:: describe
.. autofunction:: drop
.. autofunction:: init
.. autofunction:: configure
.. autofunction:: running
