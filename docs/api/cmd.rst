.. currentmodule:: pglift.cmd

Command execution
=================

.. autofunction:: run
.. autofunction:: run_expect
