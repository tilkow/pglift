.. currentmodule:: pglift.pgbackrest

Instance backup with pgBackRest
===============================

.. autofunction:: backup_command
.. autofunction:: backup
.. autofunction:: expire_command
.. autofunction:: expire
.. autoclass:: BackupType
    :members:
