Welcome to pglift's documentation!
==================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    api/index
    settings
    ansible
    dev


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
